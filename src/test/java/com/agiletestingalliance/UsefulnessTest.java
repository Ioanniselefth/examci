package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class UsefulnessTest {

    String output = "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.";

    @Test
    public void testUsefulness_Positive(){
        Usefulness usefulness = new Usefulness();
        String result = usefulness.desc();
        assertEquals(result, output);
    }

    @Test
    public void testUsefulness_Negative(){
        Usefulness usefulness = new Usefulness();
        usefulness.functionWF();
        String result = usefulness.desc();
        assertEquals(result, output);
    }
//
//
//    @Test
//    public void testUsefulness_PositiveWF(){
//        TestClass testClass = new TestClass("string");
//        String result = testClass.gstrMethod();
//        assertEquals(result, "string");
//    }
//
//    @Test
//    public void testUsefulness_NegativeWF(){
//        TestClass testClass = new TestClass("string");
//        String result = testClass.gstrMethod();
//        assertEquals(result, "string");
//    }
}
