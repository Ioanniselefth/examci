package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {

    @Test
    public void testDesc_Positive_BValue(){
        MinMax minMax = new MinMax();
        int result = minMax.fMethod(1,2);
        assertEquals(result, 2);
    }

    @Test
    public void testDesc_Positive_AValue(){
        MinMax minMax = new MinMax();
        int result = minMax.fMethod(3,1);
        assertEquals(result, 3);
    }

    @Test
    public void testBar_Positive(){
        MinMax minMax = new MinMax();
        String result = minMax.bar("string");
        assertEquals(result, "string");
    }

    @Test
    public void testBar_Negative(){
        MinMax minMax = new MinMax();
        String result = minMax.bar("string");
        assertNotEquals(result, "string2");
    }

}
