package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestClassTest {

    @Test
    public void testTestClass_Positive(){
        TestClass testClass = new TestClass("string");
        String result = testClass.gstrMethod();
        assertEquals(result, "string");
    }

    @Test
    public void testTestClass_Negative(){
        TestClass testClass = new TestClass("string");
        String result = testClass.gstrMethod();
        assertNotEquals(result, "string2");
    }

    @Test
    public void testTestClass_Negative_WithNull(){
        TestClass testClass = new TestClass();
        String result = testClass.gstrMethod();
        assertNotEquals(result,"Values should be different.");
    }
}
