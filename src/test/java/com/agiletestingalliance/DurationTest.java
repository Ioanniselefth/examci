package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class DurationTest {

    String output = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";

    @Test
    public void testDesc_Positive(){
        Duration duration = new Duration();
        String result = duration.dur();
        assertEquals(result, output);
    }

    @Test
    public void testDesc_Negative(){
        Duration duration = new Duration();
        String result = duration.dur() +"a";
        assertNotEquals(result, output);
    }

}